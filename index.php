<?php include('includes/header.php'); ?>

<body id="page-top">

	<?php include('includes/navbar.php'); ?>

	
	<div id = "part1">
	<center>
	
		<div class = "par1_1">
			<p class = "font1">Learn how YOU could<br> watch in other ways, fast.</p>
		</div>
		
		<div class = "par1_1">
			<p class = "font2">Don't want to fall in line? <b>You</b> can<br> order your ticket online
			<br>and watch hassle free.<br>
			</p>
		</div>
		
		<div class = "button_position">
				<a href="http://www.redcross.org.ph/what-we-do/social-services" class="btn btn-outline btn-xl page-scroll">Know more about us</a>
		</div>
		
		<a href = "#part2"> <img src="images/new/down_button.png" class="bounce"> </a>
		
	</center>
	</div>
	
	<?php
    function validate($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(isset($_POST['submit'])){

		$name = validate($_POST['name']);
		$address = validate($_POST['address']);
		$email = validate($_POST['email']);
		$contact = validate($_POST['contact']);
		$username = validate($_POST['username']);
		$password = validate($_POST['password']);
		$confirmpassword = validate($_POST['confirmpassword']);

        $err = 0;
		$nameErr = "";
		$addressErr = "";
		$emailErr = "";
		$contactErr = "";
		$usernameErr = "";
		$passErr = "";
		$confirmpassErr = "";
        
        if(empty($name)){
            $nameErr = "First name is required!";
            $err = 1;
        }elseif(!preg_match("/^[a-zA-Z ]*$/",$name)){
			$nameErr = "Only letters and white space allowed"; 
			$err = 1;
		}
        else{
            $err = 0;
		}
		
		if(empty($address)){
            $addressErr = "Address is required!";
            $err = 1;
        }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/",$address)){
			$addressErr = "Only letters, white spaces and numbers allowed"; 
			$err = 1;
		}
        else{
            $err = 0;
		}
		
		if(empty($email)){
            $emailErr = "Email is required!";
            $err = 1;
        }elseif(!preg_match("/[a-zA-Z0-9]@./",$email)){
			$emailErr = "Invalid E-mail Format!";
			$err = 1;
		}
        else{
            $err = 0;
		}
		
		if(empty($contact)){
			$contactErr = "Contact Number is required";
			$err= 1;
        }elseif(!preg_match("/[0-9]/",$contact)){
			$contactErr = "Invalid contact number format!";
			$err= 1;
		}
        else{
            $err = 0;
		}
		
		if(empty($username)){
            $usernameErr = "Username is required!";
            $err = 1;
        }elseif(!preg_match("/^[a-zA-Z0-9].*$/",$username) || preg_match('/\s/',$username)){
			$usernameErr = "Invalid username format"; 
			$err = 1;
		}
        else{
            $err = 0;
		}
		
		if(empty($password)){
            $passErr = "Password is required!";
            $err = 1;
		}
		elseif(strlen($password)<8){
			$passErr = "Password must have at least 8 characters!";
            $err = 1;
		}
        else{
            $err = 0;
		}
		
		if(empty($confirmpassword)){
            $confirmpassErr = "Password confirmation is required!";
            $err = 1;
		}
		elseif($password!=$confirmpassword){
			$confirmpassErr = "Passwords don't match!";
            $err = 1;
		}
        else{
            $err = 0;
		}

        if ($err == 0){
            
            session_start();
            $_SESSION['name'] = $name;
			$_SESSION['address'] = $address;
			$_SESSION['email'] = $email;
			$_SESSION['contact'] = $contact;
			$_SESSION['username'] = $username;
			$_SESSION['password'] = $password;
			$_SESSION['confirmpassword'] = $confirmpassword;

			header("Location: reserve.php");
        }
    }
	?>
	
	<div id = "part2">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
		<h1> Registration Form</h1>
	
		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
			<div class="form-group">
				<label>Name</label>
				<input name="name" type="text" class="form-control" placeholder="Name">
				<?php if(isset($nameErr)){
					echo "<div class='alert alert-danger'>$nameErr</div>";
				}
				?>

			</div>

			<div class="form-group">
				<label>Address</label>
				<input name="address" type="text" class="form-control" placeholder="Address">
				<?php if(isset($addressErr)){
					echo "<div class='alert alert-danger'>$addressErr</div>";
				}
				?>

			</div>

			<div class="form-group">
				<label>Email address</label>
				<input name="email" type="email" class="form-control" placeholder="Email">
				<?php if(isset($emailErr)){
					echo "<div class='alert alert-danger'>$emailErr</div>";
				}
				?>
		
			</div>

			<div class="form-group">
				<label>Contact Number</label>
				<input name="contact" type="text" class="form-control" placeholder="Contact Number">
				<?php if(isset($contactErr)){
					echo "<div class='alert alert-danger'>$contactErr</div>";
				}
				?>

			</div>

			<div class="form-group">
				<label>Username</label>
				<input name="username" type="text" class="form-control" placeholder="Username">
				<?php if(isset($usernameErr)){
					echo "<div class='alert alert-danger'>$usernameErr</div>";
				}
				?>

			</div>

			<div class="form-group">
				<label>Password</label>
				<input name="password" type="password" class="form-control" placeholder="Password">
				<?php if(isset($passErr)){
					echo "<div class='alert alert-danger'>$passErr</div>";
				}
				?>
			</div>

			<div class="form-group">
				<label>Confirm Password</label>
				<input name="confirmpassword" type="password" class="form-control" placeholder="Confirm Password">
				<?php if(isset($confirmpassErr)){
					echo "<div class='alert alert-danger'>$confirmpassErr</div>";
				}
				?>
			</div>

			
			<center>
			<button type="submit" name="submit" class="btn btn-success">Submit</button>  
			</center>
		</form>
		
		</div>
		<div class="col-md-4"></div>
	</div>
	</div>

		
	
<?php include('includes/footer.php'); ?>   





<?php include('includes/header.php'); ?>

<?php
    session_start();

    $fname = $_SESSION['fname'];
    $lname = $_SESSION['lname'];
    $sex = $_SESSION['sex'];
    $vol_id = $_SESSION['vol_id'];
    $address = $_SESSION['address'];
    $btype = $_SESSION['btype'];
    $contact = $_SESSION['contact'];
    $weight = $_SESSION['weight'];
    $age = $_SESSION['age'];
?>

<body id="page-top">

    <?php include('includes/navbar.php'); ?>

    <div id = "part1">
	<center>
	<br><br><br><br><br>
    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">First Name</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $fname?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Last Name</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $lname?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Sex</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $sex?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Volunteer License ID</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $vol_id?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Address</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $address?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Blood Type</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $btype?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Contact Number</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $contact?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Weight</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $weight?>" readonly>
        </div>
    </div>
    </form>

    <br>

    <form class="form-inline">
    <div class="form-group">
        <div class="input-group">
        <div class="input-group-addon">Age</div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="<?php echo $age?>" readonly>
        </div>
    </div>
    </form>
	
	</center>
	</div>
    
<?php include('includes/footer.php'); ?>
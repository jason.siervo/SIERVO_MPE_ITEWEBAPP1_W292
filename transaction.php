<?php include('includes/header.php'); ?>

<body id="page-top">

	<?php include('includes/navbar.php'); ?>

    <div id = "part1">
	<center>
	
        <div class = "par1_1">
            <p class = "font1">Learn how YOU could<br> watch in other ways, fast.</p>
        </div>

        <div class = "par1_1">
            <p class = "font2">Don't want to fall in line? <b>You</b> can<br> order your ticket online
            <br>and watch hassle free.<br>
            </p>
        </div>
		
		<div class = "button_position">
				<a href="http://www.redcross.org.ph/what-we-do/social-services" class="btn btn-outline btn-xl page-scroll">Know more about us</a>
		</div>
		
		<a href = "#part2"> <img src="images/new/down_button.png" class="bounce"> </a>
		
	</center>
    </div>
    

    <?php include('includes/footer.php'); ?>   
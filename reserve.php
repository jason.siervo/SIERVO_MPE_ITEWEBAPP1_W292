<?php include('includes/header.php'); ?>

<body id="page-top">

	<?php include('includes/navbar.php'); ?>

    <div id = "part1">
	<center>
	
		<div class = "par1_1">
			<p class = "font1">Learn how YOU could<br> watch in other ways, fast.</p>
		</div>

		<div class = "par1_1">
			<p class = "font2">Don't want to fall in line? <b>You</b> can<br> order your ticket online
			<br>and watch hassle free.<br>
			</p>
		</div>
		
		<div class = "button_position">
				<a href="http://www.redcross.org.ph/what-we-do/social-services" class="btn btn-outline btn-xl page-scroll">Know more about us</a>
		</div>
		
		<a href = "#part2"> <img src="images/new/down_button.png" class="bounce"> </a>
		
	</center>
	</div>

	<?php
	
	session_start();

	if(isset($_POST['submit'])){

		$r = $_POST['row'];
		$c = $_POST['col'];

		$err = 0;
		$rErr = "";
		$cErr = "";

		if(empty($rErr)){
            $nameErr = "Please enter a value";
            $err = 1;
        }
        else{
            $err = 0;
		}
		
		if(empty($cErr)){
            $addressErr = "Please enter a value";
            $err = 1;
        }
        else{
            $err = 0;
		}
		
		echo "<center>";
		echo "<table border= '1'>";
		for($i = 1; $i <= 10; $i++){
			echo "<tr>";
			for($j = 1; $j <= 10; $j++){
				if($i == $r && $j == $c){
					echo "<td width='50' bgcolor= 'red'> &nbsp; </td>";
				}
				else{
					echo "<td width='50'> &nbsp; </td>";
				}
			}
			echo "</tr>";
		}

		if ($err == 0){
            
            session_start();
            $_SESSION['name'] = $name;
			$_SESSION['address'] = $address;
			$_SESSION['email'] = $email;
			$_SESSION['contact'] = $contact;
			$_SESSION['username'] = $username;
			$_SESSION['password'] = $password;
			$_SESSION['confirmpassword'] = $confirmpassword;

			header("Location: transaction.php");
        }
	}	

	$name = $_SESSION['name'];
	$address = $_SESSION['address'];
	$email = $_SESSION['email'];
	$username = $_SESSION['username'];
	$contact = $_SESSION['contact'];
	$password = $_SESSION['password'];
	$confirmpassword = $_SESSION['confirmpassword'];
	?>


    <div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<center>
		    <h1>Choose a Seat</h1>

			<form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
			
				<div class="form-group">
				<label>Row</label>
				<input name="row" type="text" class="form-control" placeholder="Row">
				</div>

				<div class="form-group">
				<label>Column</label>
				<input name="col" type="text" class="form-control" placeholder="Column">
				</div>

				<input type = "hidden" name = "name" value = "<?php echo $name?>"> 
				<input type = "hidden" name = "address" value = "<?php echo $address?>"> 
				<input type = "hidden" name = "email" value = "<?php echo $email?>"> 
				<input type = "hidden" name = "username" value = "<?php echo $username?>"> 
				<input type = "hidden" name = "contact" value = "<?php echo $contact?>"> 
				<input type = "hidden" name = "password" value = "<?php echo $password?>"> 
				<input type = "hidden" name = "confirmpassword" value = "<?php echo $confirmpassword?>"> 

            <input type = "submit" name = "submit" class="btn btn-success" value = "CHOOSE"> <br>
            </form>
			<?php echo "</br>"?>

			<?php
				echo"Welcome $name! </br>";
				echo"Address: $address </br>";
				echo"Email address: $email </br>";
				echo"Username: $username </br>";
				echo"Contact: $contact </br>";
				echo"Password: $password </br>";
				echo"Confirm Password: $confirmpassword </br><br> ";
			?>
			</center>

			<center>
			<button type="submit" name="submit" class="btn btn-success">Submit</button><br><br>  
			</center>
		</div>
		<div class="col-md-4"></div>
	</div>


<?php include('includes/footer.php'); ?>   
